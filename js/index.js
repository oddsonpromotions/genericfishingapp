/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();
*/

window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);

$( document ).ready(function() {
    callApp('updates');
});

document.addEventListener ("deviceready", onDeviceReady, false);
function onDeviceReady () {
    document.body.className = device.platform.toLowerCase();
}

$('#rulesLink').on('click', function() {
    $('.content').removeClass('visible');
    $('.rules').addClass('visible');
});

$('#rulesClose').on('click', function() {
    $('.content').removeClass('visible');
    $('.more').addClass('visible');
});

var callApp = function(callId) {
    $.ajax({
        url: 'http://www.refermates.com/genericFishingApp/app.php?t='+callId,
        dataType: 'jsonp',
        jsonp: 'jsoncallback',
        timeout: 5000,
        success: function(data, status){
            $('.' + callId).html(' ');

            $.each(data, function(i,item){
                theTime = jQuery.timeago(item.time);
                var tile = '<div class="tile"><h1>'+item.title+'</h1>';

                if (item.featured_image) {
                    var tile = tile + '<img src="'+item.featured_image+'">';
                }
                var tile = tile + '<p>'+item.content+'</p><p class="timestamp">'
                + theTime+'</p></div>';

                $('.' + callId).append(tile);
            });
        },
        error: function(){
            $('.' + callId).html('Failed to load. Please try again.');
        }
    });
}

var Species = '';
$('#selectSpecies').change(function(){
    Species = $('#selectSpecies').val();
})
$('#fishTicket').on('input', function() {
    //console.log('view fish.');
    viewFish();
    $('#ticketnumber').val($(this).val());
});

$('#ticketnumber').on('input', function() {
    $('#fishTicket').val($(this).val());
});

var viewFish = function() {
    var fishTicket = $('#fishTicket').val();
    // fishTicket = parseInt(fishTicket,16).toString();
        $.ajax({
            url: 'http://www.refermates.com/genericFishingApp/viewFish.php?t='+fishTicket,
            dataType: 'jsonp',
            jsonp: 'jsoncallback',
            timeout: 5000,
            success: function(data, status){
                //console.log("weighed in fish ok.");
                $('#weighedInFish').html(' ');
                $.each(data, function(i,item){
                    var listItem = '<li><strong>'+item.species+': </strong>';
                    var listItem = listItem + item.weight + 'kg</li>';

                    $('#weighedInFish').append(listItem);
                });
            },
            error: function(){
                //console.log("weighed in fish failed.");
                $('.' + callId).html('Failed to load. Please try again.');
            }
        });

        $.ajax({
            url: 'http://www.refermates.com/genericFishingApp/viewMeasuredFish.php?t='+fishTicket,
            dataType: 'jsonp',
            jsonp: 'jsoncallback',
            timeout: 5000,
            success: function(data, status){
                //console.log("measured fish ok.");
                $('#measuredFish').html(' ');
                $.each(data, function(i,item){
                    var listItem = '<li><h4>Catch submitted ' + jQuery.timeago(item.time_entered) + ': </h4>';
                    listItem += "<p>Length: ";
                    if (item.length == null) {
                        listItem += "Processing..";
                    } else {
                        listItem += item.length + 'mm';
                    }
                    listItem += "</p></li>";
                    $('#measuredFish').append(listItem);
                });
            },
            error: function(){
                //console.log("measured failed.");
                $('.' + callId).html('Failed to load. Please try again.');
            }
        });        
}

var photoData = '';

var selectPhoto = function() {
    navigator.camera.getPicture(onSuccess, onFail, { quality: 30,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 1500,
        targetHeight: 1500
    });

    function onSuccess(imageData) {
        $('.photoPreview').show();
        var image = document.getElementById('photoSrc');
        image.src = "data:image/jpeg;base64," + imageData;
        photoData = imageData;
        $('#photoMessage').html('');
    }

    function onFail(message) {
    }
}


var takePhoto = function() {
    navigator.camera.getPicture(onSuccess, onFail, { quality: 30,
        destinationType: Camera.DestinationType.DATA_URL,
        saveToPhotoAlbum: true,
        targetWidth: 1500,
        targetHeight: 1500
    });

    function onSuccess(imageData) {
        $('.photoPreview').show();
        var image = document.getElementById('photoSrc');
        image.src = "data:image/jpeg;base64," + imageData;
        photoData = imageData;
        $('#photoMessage').html('');
    }

    function onFail(message) {
    }
}

var submitPhoto = function() {
    //check species
    if(Species==0){
        alert("Please choose species");
        return;
    }

    



    $('.atEvent').hide();
    var theTicketNumber = $('#ticketnumber').val();

    $.ajax({
        type: "POST",
        url: "http://www.refermates.com/genericFishingApp/submitPhoto.php",
        data: { ticket: theTicketNumber,
                image: photoData,
                species :Species
        }
    })
    .done(function( msg ) {
        $('.atEvent').show();
        $('#photoMessage').html( msg );
        $('.photoPreview').hide();
    });
}

$('.button').on('click', function() {
    var theId = $(this).attr('id').substring(7);
    $('.content').removeClass('visible');
    $('.' + theId).addClass('visible');
    $('.button').removeClass('current-button');
    $(this).addClass('current-button');

    $('.updates').html('<h2 class="loading">Loading...</h2>');
    $('.deals').html('<h2 class="loading">Loading...</h2>');

    if (theId === 'updates' || theId === 'deals') {
        callApp(theId);
    } else if (theId === 'fish') {
        viewFish();
    }
});

$('#selectphoto').on('click', function() {
    selectPhoto();
});

$('#takephoto').on('click', function() {
    takePhoto();
});

$('#submitPhoto').on('click', function() {
    submitPhoto();
});
